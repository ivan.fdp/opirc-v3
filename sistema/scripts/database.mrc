﻿/*
Inicializamos La BD al inciar o cargar el script
*/
ON *:START: { db_init }
ON :LOAD: { db_init }

/*
Alias de inicialización de la BD
*/
alias db_init {
  noop $db_open(opirc3.db)
  if (!%db) {
    echo 4 -a Error: %sqlite_errstr
    return
  }
  db_create settings $db_params(option UNIQUE, value)

  db_insert settings infoserver on
  db_insert settings motd on
}

/*
Alias para parametrizar los campos de la BD
*/
alias db_params {
  var %position 1, %str
  while ($evalnext($+($,%position))) {
    var %res $v1
    %str = $iif(%str, $+(%str,$chr(44)) %res, %res)
    inc %position
  }
  return $+($chr(40), %str, $chr(41))

}

/*
Alias para abrir la conexión a la BD
*/
alias db_open {
  set %db $sqlite_open($+($mircdirsistema\database\, $1))
}

/*
Alias para crear una tabla en la BD
*/
alias db_create {
  sqlite_exec %db CREATE TABLE IF NOT EXISTS $1 $2-
}

/*
Alias para insertar un campo en la tabla
*/
alias db_insert {
  tokenize 32 $1-
  ; @params: dbTable param value
  if (!%db) {
    echo 4 -a Error: No estás conectado a la db.
    return
  }

  var %safe_param = $sqlite_escape_string($2), %safe_value = $sqlite_escape_string($3-)
  sqlite_exec %db REPLACE INTO $1 (option, value) VALUES (' $+ %safe_param $+ ', ' $+ %safe_value $+ ')
}

/*
Alias para borrar el valor de un campo
*/
alias db_delete {
  if (!%db) { return }

  var %safe_param = $sqlite_escape_string($2)
  var %sql = DELETE FROM $1 WHERE option = ' $+ %safe_param $+ '
  var %request = $sqlite_exec(%db, %sql)
  if (!%request) {
    echo 4 -a Error: %sqlite_errstr
    return
  }

  sqlite_free %request
}

/*
Alias para extraer el valor de un campo
*/
alias db_get {
  if (!%db) { return }

  var %safe_param = $sqlite_escape_string($2)
  var %sql = SELECT value FROM $1 WHERE option = ' $+ %safe_param $+ '
  var %request = $sqlite_query(%db, %sql)
  if (!%request) {
    echo 4 -a Error: %sqlite_errstr
    return
  }

  if ($sqlite_num_rows(%request)) {
    var %value = $sqlite_fetch_single(%request)
    return %value
  }

  sqlite_free %request
}

/*
Alias para cerrar la conexión a la DB
*/
alias db_close {
  sqlite_close $1
  unset %db
}