
on ^1:snotice:*:{
  if (!$window(@sNotices)) {
    window -Rk0 @sNotices
    window -a @sNotices
  }
  echo -ti @sNotices $1-
  haltdef
}
;=====================================
;     Inicio
;=====================================
on *:LOAD: {
  ; Cargamos la toolbar
  loadToolBar
}
ON *:START: {
  ; Cargamos la toolbar
  loadToolBar
}

on *:EXIT: {
  dll -u $mircdirsistema\dlls\wnd.dll
}

alias loadToolbar { 
  if (!$db_get(settings, toolbarmIRC)) { noop $conf(-a, toolbarmIRC 1) }
  if (!$db_get(settings, toolbarmIRCSize)) { noop $conf(-a, toolbarmIRCSize 32) }
  var %mircToolBar $conf(-g, toolbarmIRC)
  var %mircToolBarSize $conf(-g, toolbarmIRCSize)
  mircToolBar $iif(%mircToolBar, $iif(%mircToolBarSize, $v1, 16), -r)
  channelToolbar
  queryToolbar
}

; Devuelve la ruta del icono
alias -l getIcon return $+(sistema\images\toolbar\,$1,\,$+($2,.ico))

; Control de la configuración
alias conf {
  var %switches, %error

  ; Comprobamos que tenga opciones
  if (-* iswm $1) {
    %switches = $mid($1,2-)
    tokenize 32 $2-
  }
  ; Sino existen opciones retornamos
  if (!%switches) return

  ; Comprobamos que las opciones sean correctas
  if (!$regex(%switches,^[adg]*$)) %error = SWITCH_INVALID
  elseif ($regex(%switches,([adg]).*?\1)) %error = SWITCH_DUPLICATE: $+ $regml(1)

  ; Si existe algún error retornamos
  if (%error) {
    echo -s Conf error: %error
    return
  }

  ; Ejecutamos las opciones con sus correspondientes parámetros
  var %switch 1
  while ($left($mid(%switches,%switch),1)) {
    return $do_conf($v1, $1-)
    inc %switch
  }
}
alias do_conf {
  if (!%db) { db_init }
  if ($1 == a) { noop $db_insert(settings, $2, $3-) }
  elseif ($1 == d) { $db_delete(settings, $2) }
  elseif ($1 == g) { return $db_get(settings, $2) }
}
;=====================================
;     Genera la toolbar del MiRC
;=====================================
alias mircToolbar {
  ; Comprobamos que el número de pixeles sea 16 o 32 y creamos la toolbar del MiRC
  if ($regex(size, $1, /^(16|32)$/)) {
    var %size $iif($regml(size, 1) == 16, 1, 2) ,%n = 1
    while ($toolbar(%n)) {
      var %name $toolbar(%n).name
      if (!$regex(%name, /^sep\d$/)) {
        toolbar $+(-ptz,%size) %name %name $getIcon($regml(size, 1),%name)
      }
      inc %n
    }
  }
  ; En caso contrario dejamos la toolbar del MiRC por defecto
  else { toolbar -r }
}

;=====================================
;     Alias de control para la wnd.dll
;     Vigilante de ventanas
;=====================================
alias wndll { return $dll($mircdirsistema\dlls\wnd.dll,$1,$2-) }

;=====================================
;     Control de SIGNALs de wnd.dll
;=====================================
on *:SIGNAL:Wnd: {
  var %regex $regex($window($active).type, /^(channel|query)$/)
  var %dialog $+(tb_, $regml(1))
  if (!%regex) { halt }
  ; Vigilamos que el tamaño de la ventana cambie y posicionamos la toolbar
  if ($regex($1,/^(activate|maximized|size|moving|sizing)$/)) {
    if ($dialog(%dialog).h > $window($active).h) {
      xdialog -h %dialog
      halt 
    }
    setToolbarPosition $active
  }

  ; Vigilamos el cierre de ventana
  if ($1 == tryclose && $2 == $window($active).hwnd) {
    Wndll close $2
    xdialog -h %dialog
  }
}
;=====================================
;     Genera la toolbar de canales
;=====================================
alias channelToolbar { makeToolbar channel }

;=====================================
;     Alias que setea la posición de la toolbar
;=====================================
alias setToolbarPosition {
  ; Escondemos la toolbar si la ventana activa no es un canal
  if ($regex($window($active).type, /^(channel|query)$/)) {
    var %dialog $+(tb_, $regml(1))
    var %dialogDrop $iif($regml(1) == channel, tb_query, tb_channel)
    if ($dialog(%dialogDrop)) { xdialog -h %dialogDrop }
    var %xyQuery $calc($window(@mdi).x + $window($1).x + $window($1).dw - 26)  $calc($window(@mdi).y + $window($1).y + 32)
    var %nlistLenght $calc($Window($1).w - $Window($1).dw)
    var %nlistPosition $iif($readini(mirc.ini, nicklist, $1), $gettok($readini(mirc.ini, nicklist, $1), 1, 44), $iif($gettok($readini(mirc.ini, options, n1), 29, 44) == 1, left, right))
    var %xyChannel $iif(%nlistPosition == right, $calc($window(@mdi).x + $window($1).x + $window($1).dw - 26)  $calc($window(@mdi).y + $window($1).y + 32), $calc($window(@mdi).x + $window($1).x + %nlistLenght - 26)  $calc($window(@mdi).y + $window($1).y + 32))
    var %xywh $iif($regml(1) == query, %xyQuery, %xyChannel) -1 -1
    xdialog -s %dialog
    xdialog -S %dialog %xywh
    if ($2) { scid $2 window -a $1 }
  }
  else {
    if ($dialog(tb_channel)) { xdialog -h tb_channel }
    if ($dialog(tb_query)) { xdialog -h tb_query }
  }
}

;=====================================
;     Comprobamos la ventana activa
;     para posicionar y hookear la toolbar 
;=====================================
on *:ACTIVE:*: {
  if ($regex($window($active).type, /^(channel|query)$/)) {
    Wndll hook $window($active).hwnd
    setToolbarPosition $active $cid
  }
}

;=====================================
;     Dialogo de la toolbar de canales
;=====================================
dialog tb_channel {
  size -1 -1 34 240
  option pixels
}
on *:DIALOG:tb_channel:init:*: {
  dcx Mark $dname cb_tb_channel

  ;create the toolbar
  ;Syntax: /xdialog -c [DNAME] [ID] [TYPE] [X] [Y] [W] [H] (OPTIONS)
  xdialog -c $dname 1 toolbar 0 0 24 0 list wrap left flat transparent 
  dcx xSignal 1 +st

  var %bgColor $gettok($readini(mirc.ini, palettes, n1), $calc($color(background) + 1), 44)
  xdialog -b $dname +o
  xdialog -t $dname transparentcolor none
  xdialog -g $dname +b %bgColor
  xdialog -T $dname +bp 
  xdialog -t $dname bgcolor %bgColor
  xdialog -t $dname transparentcolor %bgColor

  ;Set the Icon Size
  ;Syntax: /xdid -l [DNAME] [ID] [SIZE]
  xdid -l $dname 1 24

  ;Add an Icon to the Tooiconbars internal Icon list
  ;Syntax: /xdid -w [DNAME] [ID] [+FLAGS] [INDEX] [FILENAME]
  ;Note: this uses the shell.dll provided with dcx and assumes it is in /DCX
  echo -s $mircexe
  xdid -w $dname 1 +ndh 0 $mircdirsistema\images\channelbar\whois.ico
  xdid -w $dname 1 +ndh 0 $mircdirsistema\images\channelbar\info.ico


  ;Add a Button
  ;Syntax: /xdid -a [DNAME] [ID] [N] [+FLAGS] [WIDTH] [#ICON] [COLOR] (Button Text)[TAB](Tooltip Text)
  xdid -a $dname 1 1 +a 0 1 0
  xdid -a $dname 1 2 +a 0 2 0
}

;=====================================
;     Callback para la toolbar de canales
;=====================================
alias cb_tb_channel {
  ; echo -s [cb] $1-
  if ($3 == 1) {
    if ($2 == sclick) {
      var %comando $replace($4,1,whois,2,sinfo)
      var %nicks $iif($gettok($getSnicks($chan), 0, 32) > 0, $getSnicks($chan) , $$?="Ingresa nick")
      cmd %comando %nicks
    }
  }
}

alias getSnicks {
  var %nick 1, %nicks
  while ($snick($1, %nick)) {
    var %nicks %nicks $v1
    inc %nick
  }
  return %nicks
}

alias cmd {
  var %nick 1
  while ($gettok($2-, %nick, 32)) {
    var %cmd $v1
    var %botCmd $botCmd($1)
    $iif(%botCmd, msg $v1) $1 %cmd
    inc %nick
  }
}
alias botCmd {
  if ($regex($1, /^(sinfo)$/)) { return NiCK }
}

alias makeToolbar {
  var %dialog $+(tb_, $1)
  ; Creamos el dialogo sino existe
  if (!$dialog(%dialog)) {
    dialog -m %dialog %dialog
  }
  ; Posicionamos la toolbar
  setToolbarPosition $active
}
;=====================================
;     Genera la toolbar de privados
;=====================================
alias queryToolbar { makeToolbar query }

;=====================================
;     Dialogo de la toolbar de privados
;=====================================
dialog tb_query {
  size -1 -1 34 240
  option pixels
}

on *:DIALOG:tb_query:init:*: {
  dcx Mark $dname cb_tb_query

  ;create the toolbar
  ;Syntax: /xdialog -c [DNAME] [ID] [TYPE] [X] [Y] [W] [H] (OPTIONS)
  xdialog -c $dname 1 toolbar 0 0 24 0 list wrap left flat transparent 
  dcx xSignal 1 +st

  xdialog -b $dname +o
  xdialog -t $dname transparentcolor none
  xdialog -g $dname +b $dcx(GetSystemColor, COLOR_WINDOW)
  xdialog -T $dname +bp 
  xdialog -t $dname bgcolor $dcx(GetSystemColor, COLOR_WINDOW)
  xdialog -t $dname transparentcolor $dcx(GetSystemColor, COLOR_WINDOW)

  ;Set the Icon Size
  ;Syntax: /xdid -l [DNAME] [ID] [SIZE]
  xdid -l $dname 1 24

  ;Add an Icon to the Tooiconbars internal Icon list
  ;Syntax: /xdid -w [DNAME] [ID] [+FLAGS] [INDEX] [FILENAME]
  ;Note: this uses the shell.dll provided with dcx and assumes it is in /DCX
  echo -s $mircexe
  xdid -w $dname 1 +ndh 0 $mircdirsistema\images\channelbar\whois.ico
  xdid -w $dname 1 +ndh 0 $mircdirsistema\images\channelbar\info.ico

  ;Add a Button
  ;Syntax: /xdid -a [DNAME] [ID] [N] [+FLAGS] [WIDTH] [#ICON] [COLOR] (Button Text)[TAB](Tooltip Text)
  xdid -a $dname 1 1 +a 0 1 0
  xdid -a $dname 1 2 +a 0 2 0
}
;=====================================
;     Callback para la toolbar de canales
;=====================================
alias cb_tb_query {
  ; echo -s [cb] $1-
  if ($3 == 1) {
    if ($2 == sclick) {
      var %comando $replace($4,1,whois,2,sinfo)
      cmd %comando $active
    }
  }
}
