on *:START: { update }
; Abrimos el socket al repositorio
alias update { sockopen -e update gitlab.com 443 }

on *:SOCKOPEN:update: {
  if ($sockerr > 0 ) {
    echo -s 4ERROR: No es posible verificar si hay versiones disponibles.
    return
  }
  ; Solicitamos las tags
  sockwrite -n $sockname GET /ivan.fdp/opirc-v3/-/tags HTTP/1.1 
  sockwrite -n $sockname User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8)
  sockwrite -n $sockname Host: gitlab.com
  sockwrite -n $sockname Connection: keep-alive
  sockwrite -n $sockname $str($crlf, 2)
}
ON *:SOCKREAD:update: {
  var %data
  sockread %data
  if ($regex(version, %data, /<a class="item-title ref-name prepend-left-4" href="/ivan.fdp/opirc-v3/-/tags/.*">(.*)</a>/)) {
    ; Seteamos la última versión disponible
    set %opIRCvNew $regml(version, 1)
  }
  if ($regex(link, %data, /<a rel="nofollow" download="" class="btn btn-xs btn-primary" href="(.*)">zip</a>/)) {
    ; Seteamos el link de descarga
    set %opIRClink $regml(link, 1)
    echo -s $iif(opIRCvNew > %opIRCv, Hay una nueva versión del opIRC disponible %opIRCvNew .Descargar., Ya dispones de la última versión del opIRC disponible.)
    sockclose $sockname
  }
}

on ^*:hotlink:.*.:*:{ return }
on *:hotlink:.*.:*: {
  if ($1 == .Descargar.) { updater } 
  halt
}
; Abrimos el socket al repositorio
alias updater {
  ; Comprobamos versiones y si tenemos una antigua procesamos
  if (%opIRCvNew > %opIRCv) {
    sockopen -e updater gitlab.com 443
    write -c $+(%opIRCvNew,.zip)
  }
}

on *:SOCKOPEN:updater: {
  if ($sockerr > 0 ) {
    echo -s 4ERROR: No es posible desgargar el archivo $+(%opIRCvNew,.zip)
    return
  }
  ; Link del .zip
  sockwrite -n $sockname GET %opIRClink HTTP/1.1
  sockwrite -n $sockname Host: gitlab.com 
  sockwrite -n $sockname $str($crlf, 2)
}
ON *:SOCKREAD:updater: {
  ; Sino esta marcado leemos las variables normalmente
  if (!$sock($sockname).mark) {
    var %header
    sockread %header
    if (Content-length: * iswm %header) { %downloadlength = $gettok(%header,2,32) }
    if (%header == $null) {
      ; Marcamos el socket para indicarle que será una descarga
      sockmark $sockname $+(%opIRCvNew,.zip)
      ; Marcamos el offset de bytes
      %downloadoffset = $sock($sockname).rcvd
    }
  }
  ; Tiempo para crear el archivo binario
  else {
    sockread 4096 &data
    ; Escribimos el archivo
    bwrite $sock($sockname).mark -1 -1 &data
    if ($calc($sock($sockname).rcvd - %downloadoffset) == %downloadlength) {
      ; Si la descarga ha terminado cerramos el fichero y descomprimimos
      bwrite $sock($sockname).mark -1 -1 $crlf
      dll $qt($mircdirsistema/dlls/SZip.dll) SUnZipFile $+($mircdir, $sock($sockname).mark) > $+($mircdir, sistema\updates)
      sockclose $sockname
    }
  }

}
; Limpiando variables
alias clearData {
  unset %opIRClink
  unset %opIRCvNew
  unset %downloadlength
  unset %downloadoffset
}
ON *:signal:SZIP: { 
  if ($left($right($2,8),1) && $1 == Z_OK) {
    ; Copiamos la carpeta updates a la raiz
    run xcopy /S /Y $+($mircdir, sistema\updates\, opirc-v3-, %opIRCvNew) $mircdir
    ; Borramos la carpeta updates
    .timerDeleteFolder 1 3 deleteFolder $+($mircdir, sistema\updates\opirc-v3-, %opIRCvNew)
    ; Borramos el archivo .zip
    .timerDeleteFile 1 3 .remove $+($mircdir,%opIRCvNew,.zip)
    ; Seteamos la versión a la descargada
    set %opIRCv %opIRCvNew
    clearData
  }
}
; Alias para borrar carpetas con su contenido
alias deleteFolder {
  .comopen a Scripting.FileSystemObject
  if ($com(a,GetFolder,1,bstr*,$1-,dispatch* b)) && (!$comerr) noop $com(b,Delete,1)
  if ($com(a)) .comclose a
  if ($com(b)) .comclose b
}

menu menubar {
  .Comprobar actualizaciones: update
}

alias loadScripts {
  var %i = 1
  while ($findfile($mircdirsistema\scripts,*.mrc, %i)) {
    load -rs $v1
    inc %i
  }
}
